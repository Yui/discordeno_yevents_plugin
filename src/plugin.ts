import { Bot, event, EventHandlers, eventReturn } from "./deps.ts";
import { Tail } from "./types/mod.ts";
import { assign } from "./utils.ts";

function ignore() {}

export type transformedEvents<T extends EventHandlers> = {
  // deno-lint-ignore no-explicit-any
  [K in keyof T]: T[K] extends (...args: any) => any
    ? Parameters<T[K]>[0] extends Bot
      ? (...params: Tail<Parameters<T[K]>>) => unknown | Promise<unknown>
      : (...params: Parameters<T[K]>) => unknown | Promise<unknown>
    : never;
};

export type botWithEventEmitters<T extends Bot> = T & {
  eventEmitters: {
    on: eventReturn<transformedEvents<T["events"]>>["on"];
    off: eventReturn<transformedEvents<T["events"]>>["off"];
  };
};

export function enableEventEmitters<T extends Bot>(bot: T): botWithEventEmitters<T> {
  const emitter = event<transformedEvents<T["events"]>>();

  const on: typeof emitter.on = (type, ...params) => {
    // Only hook into dd events if the handler is not there already.
    // @ts-ignore should work fine
    if (bot.events[type].name !== "eventEmitterEvent") {
      // deno-lint-ignore no-explicit-any no-inner-declarations
      function eventEmitterEvent(...data: any[]) {
        // Remove bot
        if (Reflect.has(data[0], "token") && Reflect.has(data[0], "intents")) data.shift();

        // @ts-ignore should work fine
        emitter.emit(type, ...data);
      }

      // Hook into dd events
      // @ts-ignore should work fine
      bot.events[type] = eventEmitterEvent;
    }

    return emitter.on(type, ...params);
  };

  const off: typeof emitter.off = (type, handle) => {
    if (type === "*") {
      if (!handle) {
        for (const key of Object.keys(bot.events) as Array<keyof typeof bot.events>) {
          // Remove all events that were created by this plugin
          if (bot.events[key].name === "eventEmitterEvent") bot.events[key] = ignore;
        }
      }
    } else {
      // @ts-ignore should work fine
      if (!handle) bot.events[type] = ignore;
    }
    return emitter.off(type);
  };

  const eventEmitters = {
    eventEmitters: {
      on,
      off,
    },
  };

  assign(bot, eventEmitters);

  return bot;
}
