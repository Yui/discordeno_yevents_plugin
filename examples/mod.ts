import { createBot, GatewayIntents, startBot } from "https://deno.land/x/discordeno@17.2.0/mod.ts";
import { load } from "https://x.nest.land/Yenv@1.0.0/mod.ts";
import { enableEventEmitters } from "../src/mod.ts";

const env = await load({
  token: /[M-Z][A-Za-z\d]{23}\.[\w-]{6}\.[\w-]{27}/,
  botid: {
    type: BigInt,
    optional: true,
  },
});

const baseBot = createBot({
  token: env.token,
  intents: GatewayIntents.Guilds | GatewayIntents.GuildMessages,
  botId: env.botid ?? BigInt(atob(env.token.split(".")[0])),
});

const bot = enableEventEmitters(baseBot);

bot.eventEmitters.on("ready", (data) => {
  console.log(`Shard ${data.shardId} ready`);
});

console.log("Starting bot");
await startBot(bot);
