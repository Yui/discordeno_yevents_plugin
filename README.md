# Discordeno yevents plugin

[![nest badge](https://nest.land/badge.svg)](https://nest.land/package/discordeno_yevents_plugin)
[![Build Status](https://drone.external.sakura.yaikawa.com/api/badges/Yui/discordeno_yevents_plugin/status.svg)](https://drone.external.sakura.yaikawa.com/Yui/discordeno_yevents_plugin)

- unofficial plugin with event emitters for discordeno

- Examples available in the examples folder
